from setuptools import setup

setup(
    name='qualys_integration',
    version='0.1',
    packages=['inventory'],
    url='app.riskiq.net',
    license='BSDv2',
    author='Joseph Laycock',
    author_email='joseph.laycock@riskiq.net',
    description='Script for querying inventory assets and submitting them to qualys',
    install_requires=[
        "requests",
        "PyYAML",
        "qualysapi==5.0.3",
        "six",
        'lxml'
    ]
)
