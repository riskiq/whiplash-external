# RiskIQ Qualys integration

![Riskiq-logo](img/riskiq-logo.png)

----

# Abstract

This repo provides a set of tool to sync RiskIQ assets to Qualys

----

# Main Features

- "syncAssets": Syncing of RiskIQ inventory assets
    - Create Qualys host assets of those with a specific RiskIQ tag(s)
- "scanAssets": Syncing of RiskIQ inventory assets to an asset group(custom) and kick off a VM scan.
    - Create Qualys host assets into an asset group of those with a specific RiskIQ tag(s)
    - Perform a specified scan profile on the created asset group
- "createWebApp": Syncing of RiskIQ inventory asset (website type)
    - Create Qualys Web applications of those with a specific RiskIQ tag(s)
- "getScanResult":
    - This fetches the result of a completed scan, it can only be fetched once the scan is finished
----

# Setup RiskIQ Inventory Tags

There are two ways to do this:

- In the Web UI, select asset(s) and assign them organization, brand, or just a general tag.
- Through the API, visit https://sf.riskiq.net/crawlview/api/docs/index.html for more information.

----

## Step1. Install all necessary dependencies.

- *Recommended* Create a virtualenv to isolate this environment from your system-level Python packages However this step is not required.
- *Python3.6*

```bash
$ virtualenv venv
$ source venv/bin/activate
```

- Use `pip` (which is included with your Python3.6 install) to install the necessary dependencies.

```bash
$ python setup.py install
```

## Step2. Configure your credentials.
- Set your RiskIQ API key and secret in a file named api_config.json location in your home directory under `~/
.config/riskiq/`
example
```json
{ 
    "api_token": "api token",
    "api_private_key": "private key"
}
```
- Set your Qualys hostname, username and password in a file named qualys-settings.txt located in `~/.config/riskiq/` 
example
```conf
[info]
hostname = qualysapi.qg3.apps.qualys.com
username = username
password = password

# Set the maximum number of retries each connection should attempt. Note, this applies only to failed connections and timeouts, never to requests where the server returns a response.
max_retries = 10
```

## Step3. Running the script.

usage: 
```
$ python3 riskiq-qualys.py -h
usage: riskiq-qualys.py [-h] [-V]
                        {syncAssets,scanAssets,createWebApp,getScanResult} ...

positional arguments:
  {syncAssets,scanAssets,createWebApp,getScanResult}

optional arguments:
  -h, --help            show this help message and exit
  -V                    Enable verbose logging
```
### syncAssets
- This will create host assets in Qualys based on a given RiskIQ tag(s) and an ag_title(The asset group you would like 
these host assets to be created under)
- Simply run:

```bash
$ python riskiq-qualys.py syncAssets "<tag1>" "<tag2>" -a "<asset group>"
```
### scanAssets
- This will create an asset group of host assets in Qualys based on a given RiskIQ tag(s) and kick off a scan based 
on the scan profile given
you would like these host assets to be created under)
- Simply run:

```bash
$ python riskiq-qualys.py syncAssets "<scan profile>" "<tag1>" "<tag2>"
```
### createWebApp
- This will create web applications based on a given RiskIQ tag(s) and Qualys tag id (optional)

```bash
$ python riskiq-qualys.py createWebApp "<tag>" <12345>
```
### getScanResult
- This fetches the result of a completed scan, it can only be fetched once the scan is finished
- Simply run:

```bash
$ python riskiq-qualys.py getScanResults “<scan/id>”
```
