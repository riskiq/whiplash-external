import json
import logging
import sys
from ast import literal_eval
from lxml import etree
from qualysapi import connect

logger = logging.getLogger("riskiq.qualys.api_service")


def process_response(string: str):
    """
    Process a string to handle a potential issue with the way qualysapi work in python 3
    """
    if sys.version_info.major == 3 and string.startswith("b\'") and string.endswith("\'"):
        return literal_eval(string)
    else:
        return string


class QualysService(object):
    _output_format_ = 'xml'
    _action_path_ = {
        "trigger_vuln_scan": {
            "path": "/api/2.0/fo/scan",
            "action": "launch"
        },
        "add_asset": {
            "path": "/api/2.0/fo/asset/ip/",
            "action": "add"
        },
        "add_asset_group": {
            "path": "/api/2.0/fo/asset/group/",
            "action": "add"
        },
        "get_scan_results": {
            "path": "/api/2.0/fo/scan/",
            "action": "fetch"
        },
        "create_web_app": {
            "path": "/create/was/webapp",
            "action": "create"
        }
    }

    def __init__(self, config_path: str) -> None:
        super().__init__()
        self._agc_ = connect(config_path)

    def add_ip(self, ips, args, tracking_method="IP", comment="RiskIQ inventory integration"):
        key = 'add_asset'
        data = {
            'action': self._action_path_[key]['action'],
            'ips': ips,
            'tracking_method': tracking_method,
            'enable_vm': 1,
            'comment': comment,
            'ag_title': args.ag_title
        }
        response = self._agc_.request(self._action_path_[key]['path'], data)
        response = etree.fromstring(process_response(response))
        logger.info("API add ip response:\n%s", etree.tostring(response, encoding=str))

        return response

    def add_asset_group(self, ips: list, group_name, domains: list = None, comment="RiskIQ inventory integration"):
        key = 'add_asset_group'
        data = {
            'action': self._action_path_[key]['action'],
            'ips': ",".join(ips),
            'title': group_name,
            'domains': ",".join(domains),
            'comments': comment
        }
        response = self._agc_.request(self._action_path_[key]['path'], data)
        response = etree.fromstring(process_response(response))
        logger.info("API add asset group response:\n%s", etree.tostring(response, encoding=str))
        return response

    def trigger_vuln_scan(self, title, option="Initial Options", target=None, asset_group=None):
        key = 'trigger_vuln_scan'
        data = {
            'action': self._action_path_[key]['action'],
            'scan_title': title,
            'ip': target,
            'option_title': option,
            'asset_group_ids': asset_group
        }
        response = self._agc_.request(self._action_path_[key]['path'], data)
        response = etree.fromstring(process_response(response))
        logger.info("API response:\n%s", etree.tostring(response, encoding=str))
        return response

    def get_scan_results(self, scan_ref):
        key = 'get_scan_results'
        data = {
            'action': self._action_path_[key]['action'],
            'scan_ref': scan_ref,
            'output_format': 'json'
        }
        response = process_response(self._agc_.request(
            self._action_path_[key]['path'], data))
        logger.debug("API response:\n%s", response)
        issue = False
        try:
            response = json.loads(response)
        except json.JSONDecodeError:
            issue = True
        if issue:
            response = etree.fromstring(response)
            logger.error("There was an issue calling the sync: %s -> %s", response.find(".//CODE").text,
                         response.find(".//TEXT").text)
            return None
        logger.info("Scan_ref: %s fetched", scan_ref)
        print(json.dumps(response, indent=4, sort_keys=True))
        return response

    def create_web_app(self, domains, comment, args):  # This should take in a tag ID (found in qualys)
        key = 'create_web_app'
        if args.id:  # Passing in tag id
            data = """
                    <ServiceRequest>
                        <data>
                            <WebApp>
                                <name>{}</name>
                                <url>{}</url>
                                <tags><set>
                                <Tag><id>{}</id></Tag>
                                </set>
                                </tags>
                            </WebApp>
                        </data>
                    </ServiceRequest>
                    """.format(domains, domains, args.id)
        else:  # Without passing in the tag id
            data = """
                <ServiceRequest>
                    <data>
                        <WebApp>
                            <name>{}</name>
                            <url>{}</url>
                        </WebApp>
                    </data>
                </ServiceRequest>
                """.format(domains, domains)

        response = process_response(self._agc_.request(
            self._action_path_[key]['path'], data))
        response: str = etree.fromstring(response)
        logger.info("API response:\n%s", etree.tostring(response, pretty_print=True, encoding=str))
        return response
