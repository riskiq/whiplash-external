import logging
import os
from argparse import ArgumentParser
from datetime import datetime
from itertools import groupby
from lxml import etree
from inventory import Config, InventoryRepo, build_filters, CONFIG_PATH
from qualys import QualysService


def extract_ip(it):
    return it.get('host').get('ipAddress')


def extract_host(it):
    return it.get('host').get('host')


def extract_domain(it):
    return it.get('domain').get('domain')


def extract_url(it):
    return it.get('name')


def sync_websites(websites: list, qs: QualysService, comment: str, arg_val):
    logger.info("Syncing WEB_SITE: %s", len(websites))
    response = qs.add_ip(",".join(list(set(map(extract_ip, websites)))), tracking_method="DNS",
                         comment=comment, args=arg_val)
    return response


def sync_hosts(websites: list, qs: QualysService, comment: str, arg_val):

    logger.info("Syncing HOST: %s", len(websites))
    response = qs.add_ip(",".join(list(set(map(extract_ip, websites)))), tracking_method="DNS",
                         comment=comment, args=arg_val)
    return response


def sync_web_apps(websites: list, qs: QualysService, comment: str, arg_val):
    logger.info("Syncing WEB_APP: %s", len(websites))
    for website in websites:
        qs.create_web_app(extract_url(website), comment=comment, args=arg_val)


def sync_ip(websites: list, qs: QualysService, comment: str, args):
    logger.info("Syncing IP: %s", len(websites))
    response = qs.add_ip(",".join(list(set(map(extract_ip, websites)))),
                         comment=comment, args=args)
    return response


def parse_sync_response(response: str = None):
    code = response.find(".//CODE")
    return code is None


def sync_assets(arg_val):
    repo = InventoryRepo(config)
    qs = QualysService(config.get_configuration("qualys_config_path"))
    # Config is instantiated at the bottom
    assets = repo.scroll_assets(build_filters(arg_val.tags, asset_type='vm'))  # Retrieve assets from RiskIQ
    asset_responses = list(assets)
    logger.info("Syncing %s assets", len(asset_responses))
    asset_responses.sort(key=lambda it: it['assetType'])
    comment = "[{}] RiskIQ automatic inventory sync. Tags{}".format(str(datetime.now()).split('.')[0], arg_val.tags)
    if asset_responses:
        for k, v in groupby(asset_responses, key=lambda it: it['assetType']):
            # depending on asset type, asset_sync_method picks up the sync_ip/host/website function
            response = asset_sync_method.get(k, lambda *it: None)(list(v), qs, comment, arg_val)  # *it allows it to
            # take in multiple arguments so it doesn't throw an error.
            if not parse_sync_response(response):
                logger.error("There was an issue calling the sync: %s -> %s", response.find(".//CODE").text,
                             response.find(".//TEXT").text)
                break
    else:
        logger.error("No website in inventory with tag: %s", *arg_val.tags)


def scan_assets(args):
    repo = InventoryRepo(config)
    qs = QualysService(config.get_configuration("qualys_config_path"))
    assets = repo.scroll_assets(build_filters(args.tags, asset_type='vm'))  # Retrieve assets from RiskIQ
    targets = list(assets)
    targets.sort(key=lambda it: it['assetType'])
    hosts, ips = list(), list()
    logger.info("Scanning %s assets", len(targets))
    for k, v in groupby(targets, key=lambda it: it['assetType']):
        values = list(v)
        if k != "IP":
            hosts.extend(map(extract_domain, values))
        ips.extend(map(extract_ip, values))
    logger.info("Hosts in asset group: %s", hosts)
    logger.info("IPs in asset group: %s", ips)

    comment = "Tags{}".format(args.tags)
    label = "[{}] RiskIQ inventory scan.".format(
        str(datetime.now()).split('.')[0])
    response = qs.add_asset_group(ips=(list(set(ips))),
                                  group_name=label,
                                  domains=list(), comment=comment)
    logger.debug("API response:\n%s", etree.tostring(response, encoding=str))  # Create an asset group with IPs in it
    if not parse_sync_response(response):
        logger.error("There was an issue calling the sync: %s -> %s", response.find(".//CODE").text,
                     response.find(".//TEXT").text)
        return
    target_group_id = response.find(".//VALUE").text
    logger.info("Triggering a scan on target group id: %s", target_group_id)
    response = qs.trigger_vuln_scan(
        label, asset_group=target_group_id, option=' '.join(args.option_group_name).replace("+", " "))  # Use the
    # asset group ID to kick off a scan
    if not parse_sync_response(response):
        logger.error("There was an issue calling the sync: %s -> %s", response.find(".//CODE").text,
                     response.find(".//TEXT").text)
        return
    scan_ref = response.find(".//ITEM_LIST/ITEM[last()]/VALUE").text
    logger.info("Scan ref is: %s", scan_ref)


def get_scan_result(args):
    qs = QualysService(config.get_configuration("qualys_config_path"))
    if qs.get_scan_results(args.scan_ref):
        return
    else:
        logger.info("There was an error")


def create_web_app(args):
    qs = QualysService(config.get_configuration("qualys_config_path"))
    repo = InventoryRepo(config)
    comment = "[{}] RiskIQ automatic inventory sync. Tags{}".format(str(datetime.now()).split('.')[0],
                                                                    *args.tag)
    assets = repo.scroll_assets(build_filters(args.tag, asset_type='was'))  # Retrieve assets from RiskIQ
    asset_responses = list(assets)
    if asset_responses:
        for k, v in groupby(asset_responses, key=lambda it: it['assetType']):
            try:
                asset_sync_method.get("WAS")(list(v), qs, comment, args)
            except Exception as e:
                logger.debug('[ERROR] {}'.format(e))
    else:
        logger.error("No website in inventory with tag: %s", *args.tag)


"""
Paths for actions, methods, configurations.
"""

__commands__ = {
    'syncAssets': sync_assets,
    'scanAssets': scan_assets,
    'getScanResult': get_scan_result,
    'createWebApp': create_web_app
}
asset_sync_method = {
    "WEB_SITE": sync_websites,
    "HOST": sync_hosts,
    "IP": sync_ip,
    "WAS": sync_web_apps
}

default_config = {
    "api_server": "ws.riskiq.net",
    "api_version": "v1",
    "response_size": 51,
    "qualys_config_path": os.path.join(CONFIG_PATH, 'qualys-settings.conf')
}

required_configurations = [
    'api_token',
    'api_private_key',
    'api_server',
    "api_version",
    'response_size',
    'qualys_config_path'
]

config = Config(required=required_configurations, defaults=default_config)  # Instantiating the Config class
logger = logging.getLogger("riskiq.inventory.sync")

if __name__ == '__main__':
    #  Setting logger
    logging.basicConfig(level=logging.INFO,
                        format='%(name)-12s: %(levelname)-8s %(message)s')
    logging.getLogger('qualysapi.connector').setLevel(logging.INFO)
    args = ArgumentParser(
        description="Submit inventory assets for Qualys scan.")
    args.add_argument('-V', help="Enable verbose logging", dest="verbose", const=True, default=False,
                      action="store_const")
    sub_parsers = args.add_subparsers(dest="command")
    sync = sub_parsers.add_parser("syncAssets")
    sync.add_argument('-a', metavar="ag_title", dest="ag_title", help='Asset_group to add the assets to')
    sync.add_argument('tags', help='Tags to filter by', nargs="+")

    scan = sub_parsers.add_parser("scanAssets")
    scan.add_argument('option_group_name', nargs=1, metavar="options",
                      help='The qualys option scan profile')
    scan.add_argument('tags', help='Tags to filter by', nargs="+")

    create_web_app = sub_parsers.add_parser("createWebApp")
    create_web_app.add_argument('-i', dest="id", help='Optional Qualys tag id')
    create_web_app.add_argument('tag', metavar='tags', help='RiskIQ tag to filter by', nargs="+")

    fetch = sub_parsers.add_parser("getScanResult")
    fetch.add_argument('scan_ref', metavar="reference")
    val = args.parse_args()
    if val.verbose:
        logging.getLogger('riskiq').setLevel(logging.DEBUG)
    __commands__[val.command](val)
