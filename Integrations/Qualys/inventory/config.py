import json
import os 

CONFIG_PATH = os.path.expanduser('~/.config/riskiq')
CONFIG_FILE = os.path.join(CONFIG_PATH, 'api_config.json')


class Config(object):
    _json_: json

    def __init__(self, required: list, defaults: dict, file_name: str = CONFIG_FILE) -> None:
        super().__init__()
        self._required_ = required
        self._defaults_ = defaults
        with open(file_name, 'r') as jsonFile:
            self._json_ = json.load(jsonFile) if not None else dict()
        props_to_check = []
        for props in self._required_:
            props_to_check.append(props) if self.get_configuration(props) is None else None
        if len(props_to_check) > 0:
            raise Exception('These properties need to be configured: {}'.format(props_to_check))

    def get_configuration(self, name: str, default: object = None):
        val = self._json_.get(name, default)
        if val is not None:
            return val
        return self._defaults_.get(name, None)  # Returns the "Qualys_config_path"
