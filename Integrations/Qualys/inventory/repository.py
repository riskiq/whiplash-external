import json
import logging
import os
from requests import post
from .config import Config

CUR_PATH = os.getcwd()

logger = logging.getLogger("riskiq.inventory.repository")


def request_params_factory(config: Config) -> dict:
    params = dict()
    headers = {
        "X-RiskIQ-Client": "PY-Inventory-Lib",
        'accept': "application/json"
    }
    params['headers'] = headers
    auth = (config.get_configuration("api_token"),
            config.get_configuration("api_private_key"))
    params['auth'] = auth
    return params


def build_filters(tags: tuple, asset_type):
    search_params = dict()
    if asset_type == 'ip' or asset_type == 'vm':
        with open(CUR_PATH + "/inventory/JSON/" + "asset.json") as param:
            search_params = json.load(param)
            print(search_params)
    if asset_type == 'was':
        with open(CUR_PATH + "/inventory/JSON/" + "website_asset.json") as param:
            search_params = json.load(param)
    tags = ",".join(tags)
    param = {
        "filters": [{
            "field": "tag",
            "value": tags,
            "type": "IN"
        }]
    }
    search_params['filters'].append(param)
    return search_params


class InventoryRepo(object):
    _ws_url_base_: str

    def __init__(self, config: Config) -> None:
        super().__init__()
        api_server=config.get_configuration("api_server")
        api_version=config.get_configuration("api_version")
        self._ws_url_base_ = "https://"+api_server+"/"+api_version+"/"
        logger.debug(self._ws_url_base_)
        self._config_ = config

    def scroll_assets(self, filters: list) -> iter:
        return AssetScroll(self, filters)

    def search_assets(self, filters: list, scroll: str = '') -> dict:
        params = dict()
        params['results'] = self._config_.get_configuration("response_size")
        params['scroll'] = scroll
        post_data = filters
        response = json.loads(post(self._ws_url_base_ + "/inventory/search", json=post_data, params=params,
                                   **request_params_factory(self._config_)).content)
        logger.debug("API response:\n%s", response)
        return response

    def update_assets(self, assets: list, changes: dict):
        asset_ids = [asset['assetID']
                     for asset in assets if 'assetID' in asset]
        changes['ids'] = asset_ids
        response = json.loads(post(self._ws_url_base_ + "/inventory/update", json=changes,
                                   **request_params_factory(self._config_)).content)
        logger.debug("API response:\n%s", response)
        return response


class AssetScroll(object):
    _filters_: list
    _first_call_ = True
    _repo_: InventoryRepo
    _next_scroll_id_: str = None
    _iter_ = None
    _consumed_ = False

    def __init__(self, repo: InventoryRepo, filters: list) -> None:
        super().__init__()
        self._filters_ = filters
        self._current_list_ = list()
        self._repo_ = repo

    def __iter__(self):
        if self._consumed_:
            raise self.ScrollException("Cannot consume the same scroll twice")
        return self

    def __next__(self):
        if self._first_call_:
            self._first_call_ = False
            self._iter_ = self._get_asset_list(
                self._repo_.search_assets(self._filters_)).__iter__()
        try:
            next_item = self._iter_.__next__()
        except StopIteration:
            self._iter_ = self._get_asset_list(
                self._repo_.search_assets(self._filters_, self._next_scroll_id_)).__iter__()
            next_item = self._iter_.__next__()
        return next_item

    def _get_asset_list(self, response: dict) -> list:
        if "scroll" in response.keys():
            self._next_scroll_id_ = response["scroll"]
        else:
            self._next_scroll_id_ = None
        if response.get('errorCode', None) is not None:
            raise self.ScrollException("There was an exception in the request: {}".format(response))

        results = response.get("inventoryAsset")
        if len(results) == 0 or self._next_scroll_id_ is None:
            self._consumed_ = True
            raise StopIteration
        return results

    class ScrollException(Exception):
        pass
